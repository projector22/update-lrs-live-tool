# Update LRS Live Tool

## v0.1.1

### Added

- Update for dev branch.

---

## v0.1.0

### Added

- Moved over from `Linux New System`.
